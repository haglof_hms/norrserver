#pragma once

class CLicProtectorEasyGo270;

struct LICENCE
{
	DWORD time;
	CString user;
};


bool EnterCriticalSection(int &semaphore);
void LeaveCriticalSection(int &semaphore);

bool GetConn(DB_CONNECTION_DATA &conn);
CString GetTempFilePath();
CString GetAppDir();

void WriteToEventLog(WORD type, LPCTSTR msg);

void SetLastMessage(LPCTSTR msg);
bool UpdateLastMessage(HMODULE hModule);

bool _CheckLicence(LPCTSTR user);

typedef void (*GetMessageLogFunction)(CString &log);


static LPCTSTR BasicKey = _T("2853657132");
static LPCTSTR LicenceFile = _T("NorrServer.lic");
static LPCTSTR ModuleId = _T("H9004");
static DWORD LicenceTimeout = 15*60*1000; // In milliseconds

extern CLicProtectorEasyGo270 g_proxyLicProtector;
