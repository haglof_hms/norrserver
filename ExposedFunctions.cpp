#include "stdafx.h"
#include "MainFrm.h"
#include "Utilities.h"
#include "Intermediate/NorrServer_h.h"
#include "../NorrClient/NorrClient/TemplateParser.h"

typedef BOOL (*ImportFileFunction)(CString file, LPCTSTR templateXml, LPCTSTR username, DB_CONNECTION_DATA &conn, int* standId);
typedef BOOL (*CalculateStandFunction)(int standId, DB_CONNECTION_DATA &conn);
typedef BOOL (*ChangeTemplateFixFunction)(int trakt_id, LPCTSTR templateXml, LPCTSTR pricelistXml, DB_CONNECTION_DATA &conn);

extern CString g_strLastMessage; // Hidden global defined in Utilities.cpp (we don't want any functions messing with this string)
int g_nLock = 0; // Locking semaphor

// Server functions
boolean CreateStand(const char* pData, const wchar_t* pTemplateXml, const wchar_t* pUsername, int* nStandId)
{
	bool ret = false;
	DB_CONNECTION_DATA conn;
	CFile file;
	CString filepath;
	int tries;

	// Put data in a temp file
	tries = 0;
	while( file.m_hFile == CFile::hFileNull )
	{
		filepath = GetTempFilePath(); DeleteFile(filepath); // Delete .tmp file
		filepath.Replace(_T(".tmp"), _T(".hxl"));
		file.Open(filepath, CFile::modeCreate | CFile::modeReadWrite);

		if( ++tries > 2 )
		{
			CString str; str.Format(_T("Det gick inte att �ppna filen f�r skrivning '%s'."), filepath);
			SetLastMessage(str);
			return false;
		}
	}
	file.Write(pData, strlen(pData));
	file.Close();

	// Make sure we process only one request at a time
	if( !EnterCriticalSection(g_nLock) )
	{
		return false;
	}

	// Load indata module
	HINSTANCE hModule = AfxLoadLibrary(_T("Modules\\UMIndata.dll"));
	if( hModule )
	{
		ImportFileFunction func = (ImportFileFunction)GetProcAddress((HMODULE)hModule, "ImportFile");
		if( func )
		{
			if( GetConn(conn) )
			{
				// Let indata module handle import
				if( func(filepath, pTemplateXml, pUsername, conn, nStandId) )
				{
					ret = true;
				}
				UpdateLastMessage(hModule);
			}
		}
		else
		{
			SetLastMessage(_T("Funktionen f�r att l�sa in best�nd hittades inte i modulen."));
		}

		AfxFreeLibrary(hModule);
	}
	else
	{
		SetLastMessage(_T("Modulen UMIndata kunde inte l�sas in."));
	}

	// Remove temp file
	DeleteFile(filepath);

	LeaveCriticalSection(g_nLock);

	return ret;
}

boolean CalculateStand(const int nStandId)
{
	bool ret = false;
	CString log;
	DB_CONNECTION_DATA conn;

	// Make sure we process only one request at a time
	if( !EnterCriticalSection(g_nLock) )
	{
		return false;
	}

	// Load estimate module
	HINSTANCE hModule = AfxLoadLibrary(_T("Modules\\UMEstimate.dll"));
	if( hModule )
	{
		CalculateStandFunction func = (CalculateStandFunction)GetProcAddress((HMODULE)hModule, "calculateStand");
		if( func )
		{
			if( GetConn(conn) )
			{
				// Do calculation
				if( func(nStandId, conn) )
				{
					ret = true;
				}
				UpdateLastMessage(hModule);
			}
		}
		else
		{
			SetLastMessage(_T("Funktionen f�r att ber�kna best�nd hittades inte i modulen."));
		}

		AfxFreeLibrary(hModule);
	}
	else
	{
		SetLastMessage(_T("Modulen UMEstimate kunde inte l�sas in."));
	}

	LeaveCriticalSection(g_nLock);

	return ret;
}

boolean ChangeTemplate(const int nStandId, const char* pTemplateXml)
{
	bool ret = false;
	bool exist;
	CString pricelistXml;
	DB_CONNECTION_DATA conn;
	POSITION pos;
	SPECIE spc;

	// Parse stand template
	CTemplateParser tmpl;
	if( !tmpl.ParseXml(pTemplateXml) )
		return false;

	if( !GetConn(conn) )
		return false;

	// Make sure this template include all neccessary species
	try
	{
		SACommand cmd(conn.conn, _T("SELECT DISTINCT tdata_spc_id FROM esti_trakt_data_table WHERE tdata_trakt_id = :1"));
		cmd.Param(1).setAsLong() = nStandId;
		cmd.Execute();

		SpecieList &spclst = tmpl.GetSpecieList();
		while( cmd.FetchNext() )
		{
			exist = false;
			pos = spclst.GetHeadPosition();
			while( pos )
			{
				spc = spclst.GetNext(pos);
				if( spc.id == cmd.Field(1).asLong() )
				{
					exist = true;
					break;
				}
			}

			if( !exist )
			{
				SetLastMessage(_T("Mallen saknar tr�dslag som anv�nds i best�ndet."));
				return false;
			}
		}
	}
	catch( SAException &e )
	{
		SetLastMessage(e.ErrText());
		return false;
	}

	// Make sure we process only one request at a time
	if( !EnterCriticalSection(g_nLock) )
	{
		return false;
	}

	// Load estimate module
	HINSTANCE hModule = AfxLoadLibrary(_T("Modules\\UMEstimate.dll"));
	if( hModule )
	{
		ChangeTemplateFixFunction func = (ChangeTemplateFixFunction)GetProcAddress((HMODULE)hModule, "changeTemplateFix");
		if( func )
		{
			try
			{
				SACommand cmd(conn.conn, _T(""));

				// Pricelist
				cmd.setCommandText(_T("UPDATE esti_trakt_misc_data_table SET tprl_name = prl.name, tprl_typeof = prl.type_of, tprl_pricelist = prl.pricelist FROM prn_pricelist_table prl WHERE tprl_trakt_id = :1 AND prl.id = :2"));
				cmd.Param(1).setAsLong() = nStandId;
				cmd.Param(2).setAsLong() = tmpl.GetPricelistId();
				cmd.Execute();

				// Cost template
				cmd.setCommandText(_T("UPDATE esti_trakt_misc_data_table SET tprl_costtmpl_name = cost_name, tprl_costtmpl_typeof = cost_type_of, tprl_costtmpl = cost_template FROM cost_template_table WHERE tprl_trakt_id = :1 AND cost_id = :2"));
				cmd.Param(1).setAsLong() = nStandId;
				cmd.Param(2).setAsLong() = tmpl.GetCostTemplateId();
				cmd.Execute();

				// Stand settings
				cmd.setCommandText(_T("UPDATE esti_trakt_table SET trakt_near_coast = :1, trakt_southeast = :2, trakt_region5 = :3, trakt_part_of_plot = :4, trakt_si_h100_pine = :5 WHERE trakt_id = :6"));
				cmd.Param(1).setAsLong() = tmpl.GetAtCoast();
				cmd.Param(2).setAsLong() = tmpl.GetSouthEast();
				cmd.Param(3).setAsLong() = tmpl.GetRegion5();
				cmd.Param(4).setAsLong() = tmpl.GetPartOfPlot();
				cmd.Param(5).setAsString() = tmpl.GetH100Pine();
				cmd.Param(6).setAsLong() = nStandId;
				cmd.Execute();

				// Specie settings
				cmd.setCommandText(_T("DELETE FROM esti_trakt_set_spc_table WHERE tsetspc_tdata_trakt_id = :1"));
				cmd.Param(1).setAsLong() = nStandId;
				cmd.Execute();

				SpecieList &spclst = tmpl.GetSpecieList();
				pos = spclst.GetHeadPosition();
				while( pos )
				{
					spc = spclst.GetNext(pos);

					// Make sure specie exist in stand
					exist = false;
					cmd.setCommandText(_T("SELECT 1 FROM esti_trakt_data_table WHERE tdata_id = :1 AND tdata_trakt_id = :2 AND tdata_data_type = :3"));
					cmd.Param(1).setAsLong() = spc.id;
					cmd.Param(2).setAsLong() = nStandId;
					cmd.Param(3).setAsLong() = 1; // STMP_LEN_WITHDRAW (Uttag)
					cmd.Execute();
					while( cmd.FetchNext() ) exist = true;
					if( !exist ) continue;

					// Update specie settings
					cmd.setCommandText(_T("UPDATE esti_trakt_data_table SET tdata_h25 = :1, tdata_gcrown = :2 WHERE tdata_spc_id = :3 AND tdata_trakt_id = :4"));
					cmd.Param(1).setAsDouble() = spc.h25;
					cmd.Param(2).setAsDouble() = spc.gcrown;
					cmd.Param(3).setAsLong() = spc.id;
					cmd.Param(4).setAsLong() = nStandId;
					cmd.Execute();

					cmd.setCommandText(_T("INSERT INTO esti_trakt_set_spc_table (tsetspc_id,")
																			 _T("tsetspc_tdata_id,")
																			 _T("tsetspc_tdata_trakt_id,")
																			 _T("tsetspc_data_type,")
																			 _T("tsetspc_specie_id,")
																			 _T("tsetspc_hgt_func_id,")
																			 _T("tsetspc_hgt_specie_id,")
																			 _T("tsetspc_hgt_func_index,")
																			 _T("tsetspc_vol_func_id,")
																			 _T("tsetspc_vol_specie_id,")
																			 _T("tsetspc_vol_func_index,")
																			 _T("tsetspc_bark_func_id,")
																			 _T("tsetspc_bark_specie_id,")
																			 _T("tsetspc_bark_func_index,")
																			 _T("tsetspc_vol_ub_func_id,")
																			 _T("tsetspc_vol_ub_specie_id,")
																			 _T("tsetspc_vol_ub_func_index,")
																			 _T("tsetspc_m3sk_m3ub,")
																			 _T("tsetspc_m3sk_m3fub,")
																			 _T("tsetspc_m3fub_m3to,")
																			 _T("tsetspc_dcls,")
																			 _T("tsetspc_qdesc_index,")
																			 _T("tsetspc_extra_info,")
																			 _T("created,")
																			 _T("tsetspc_transp_dist,")
																			 _T("tsetspc_transp_dist2,")
																			 _T("tsetspc_qdesc_name,")
																			 _T("tsetspc_grot_func_id,")
																			 _T("tsetspc_grot_percent,")
																			 _T("tsetspc_grot_price,")
																			 _T("tsetspc_grot_cost)")
									   _T("VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,'',CURRENT_TIMESTAMP,:23,:24,:25,:26,:27,:28,:29)"));
					cmd.Param(1).setAsLong() = spc.id;
					cmd.Param(2).setAsLong() = spc.id;
					cmd.Param(3).setAsLong() = nStandId;
					cmd.Param(4).setAsShort() = 1; // STMP_LEN_WITHDRAW (Uttag)
					cmd.Param(5).setAsLong() = spc.id;
					cmd.Param(6).setAsLong() = spc.hgtId;
					cmd.Param(7).setAsLong() = spc.hgtSpcId;
					cmd.Param(8).setAsLong() = spc.hgtIdx;
					cmd.Param(9).setAsLong() = spc.volId;
					cmd.Param(10).setAsLong() = spc.volSpcId;
					cmd.Param(11).setAsLong() = spc.volIdx;
					cmd.Param(12).setAsLong() = spc.barkId;
					cmd.Param(13).setAsLong() = spc.barkSpcId;
					cmd.Param(14).setAsLong() = spc.barkIdx;
					cmd.Param(15).setAsLong() = spc.ubId;
					cmd.Param(16).setAsLong() = spc.ubSpcId;
					cmd.Param(17).setAsLong() = spc.ubIdx;
					cmd.Param(18).setAsDouble() = spc.sk2ub;
					cmd.Param(19).setAsDouble() = spc.sk2fub;
					cmd.Param(20).setAsDouble() = spc.fub2to;
					cmd.Param(21).setAsDouble() = tmpl.GetDcls();
					cmd.Param(22).setAsLong() = spc.qualityId;
					cmd.Param(23).setAsLong() = spc.dist1;
					cmd.Param(24).setAsLong() = spc.dist2;
					cmd.Param(25).setAsString() = spc.qualityName;
					cmd.Param(26).setAsLong() = spc.grotId;
					cmd.Param(27).setAsDouble() = spc.grotPercent;
					cmd.Param(28).setAsDouble() = spc.grotPrice;
					cmd.Param(29).setAsDouble() = spc.grotCost;
					cmd.Execute();
				}

				// Remove existing assortments
				cmd.setCommandText(_T("DELETE FROM esti_trakt_spc_assort_table WHERE tass_trakt_id = :1"));
				cmd.Param(1).setAsLong() = nStandId;
				cmd.Execute();

				// Remove existing transfers
				cmd.setCommandText(_T("DELETE FROM esti_trakt_trans_assort_table WHERE ttrans_trakt_id = :1"));
				cmd.Param(1).setAsLong() = nStandId;
				cmd.Execute();

				// Get pricelist xml
				cmd.setCommandText(_T("SELECT pricelist FROM prn_pricelist_table WHERE id = :1"));
				cmd.Param(1).setAsLong() = tmpl.GetPricelistId();
				cmd.Execute();
				exist = false;
				while( cmd.FetchNext() )
				{
					pricelistXml = cmd.Field(1).asString();
					exist = true;
				}

				// Fix the rest in UMEstimate (specie assortments and transfers)
				if( exist )
				{
					if( func(nStandId, CString(pTemplateXml), pricelistXml, conn) )
					{
						ret = true;
					}
					else
					{
						SetLastMessage(_T("Byte av kunde inte slutf�ras, fel i UMEstimate."));
					}
					UpdateLastMessage(hModule);
				}
				else
				{
					SetLastMessage(_T("Tillh�rande prislista hittades inte."));
				}
			}
			catch( SAException &e )
			{
				SetLastMessage(e.ErrText());
			}
		}
		else
		{
			SetLastMessage(_T("Funktionen f�r att byta best�ndsmall hittades inte i modulen."));
		}
	}
	else
	{
		SetLastMessage(_T("Modulen UMEstimate kunde inte l�sas in."));
	}

	LeaveCriticalSection(g_nLock);

	return ret;
}

boolean GetStandArray(const int nDesiredSize, int* nActualSize, STAND* pData)
{
	bool ret = false;
	int ct;
	DB_CONNECTION_DATA conn;

	*nActualSize = 0;

	try
	{
		if( GetConn(conn) )
		{
			// Get data for all stands
			SAString sql(_T("SELECT trakt_id, trakt_num, trakt_name, trakt_date, trakt_areal FROM esti_trakt_table"));
			SACommand cmd(conn.conn, sql);
			cmd.Execute();
			ct = 0;
			while( cmd.FetchNext() )
			{
				// Make sure we don't overrun the buffer (a stand might have been created or removed since the client call to GetStandCount)
				if( ct >= nDesiredSize ) break;

				pData[ct].id	= cmd.Field(1).asLong();
				pData[ct].area	= cmd.Field(5).asDouble();
				wcscpy_s(pData[ct].num, sizeof(pData[ct].num) / sizeof(wchar_t), cmd.Field(2).asString());
				wcscpy_s(pData[ct].name, sizeof(pData[ct].name) / sizeof(wchar_t), cmd.Field(3).asString());
				wcscpy_s(pData[ct].date, sizeof(pData[ct].date) / sizeof(wchar_t), cmd.Field(4).asString());
				ct++;
			}

			// Return number of records
			*nActualSize = ct;
			ret = true;
		}
	}
	catch( SAException &e )
	{
		SetLastMessage(e.ErrText());
	}

	return ret;
}

boolean GetStandCount(int* nCount)
{
	bool ret = false;
	DB_CONNECTION_DATA conn;

	*nCount = 0;

	try
	{
		if( GetConn(conn) )
		{
			// Determine how much memory we need
			SAString sql(_T("SELECT COUNT(1) FROM esti_trakt_table"));
			SACommand cmd(conn.conn, sql);
			cmd.Execute();
			while(cmd.FetchNext()) // A while-statement is used intentionally here (workaround for a bug in SQLAPI)
			{
				*nCount = cmd.Field(1).asLong();
			}
			ret = true;
		}
	}
	catch( SAException &e )
	{
		SetLastMessage(e.ErrText());
	}

	return ret;
}

boolean GetTemplateArray(const int nDesiredSize, int* nActualSize, TEMPLATE* pData)
{
	bool ret = false;
	int ct;
	DB_CONNECTION_DATA conn;

	*nActualSize = 0;

	try
	{
		if( GetConn(conn) )
		{
			// Get data for all stands
			SAString sql(_T("SELECT tmpl_id, tmpl_name FROM tmpl_template_table WHERE tmpl_type_of > 0"));
			SACommand cmd(conn.conn, sql);
			cmd.Execute();
			ct = 0;
			while( cmd.FetchNext() )
			{
				// Make sure we don't overrun the buffer
				if( ct >= nDesiredSize ) break;

				pData[ct].id = cmd.Field(1).asLong();
				wcscpy_s(pData[ct].name, sizeof(pData[ct].name) / sizeof(wchar_t), cmd.Field(2).asString());
				ct++;
			}

			// Return number of records
			*nActualSize = ct;
			ret = true;
		}
	}
	catch( SAException &e )
	{
		SetLastMessage(e.ErrText());
	}

	return ret;
}

boolean GetTemplateCount(int* nCount)
{
	bool ret = false;
	DB_CONNECTION_DATA conn;

	*nCount = 0;

	try
	{
		if( GetConn(conn) )
		{
			// Determine how much memory we need
			SAString sql(_T("SELECT COUNT(1) FROM tmpl_template_table WHERE tmpl_type_of > 0"));
			SACommand cmd(conn.conn, sql);
			cmd.Execute();
			while(cmd.FetchNext()) // A while-statement is used intentionally here (workaround for a bug in SQLAPI)
			{
				*nCount = cmd.Field(1).asLong();
			}
			ret = true;
		}
	}
	catch( SAException &e )
	{
		SetLastMessage(e.ErrText());
	}

	return ret;
}

boolean GetTemplateXml(const int nTemplateId, const int nDesiredSize, int* nActualSize, wchar_t* pData)
{
	bool ret = false;
	DB_CONNECTION_DATA conn;

	try
	{
		if( GetConn(conn) )
		{
			// Get template xml string, if buffer isn't large enough to return it, then return the size of the string (plus one for null-termination)
			SAString sql; sql.Format(_T("SELECT tmpl_template FROM tmpl_template_table WHERE tmpl_id = %d"), nTemplateId);
			SACommand cmd(conn.conn, sql);
			cmd.Execute();
			while(cmd.FetchNext()) // Intentional while loop
			{
				if( nDesiredSize > cmd.Field(1).asString().GetLength() )
				{
					wcscpy_s(pData, nDesiredSize, cmd.Field(1).asString());
					ret = true;
				}

				// Always return actual size (plus null-term char)
				*nActualSize = cmd.Field(1).asString().GetLength() + 1;
			}
		}
	}
	catch( SAException &e )
	{
		SetLastMessage(e.ErrText());
	}

	return ret;
}

boolean GetPricelistXml(const int nPricelistId, const int nDesiredSize, int* nActualSize, wchar_t* pData)
{
	bool ret = false;
	DB_CONNECTION_DATA conn;

	try
	{
		if( GetConn(conn) )
		{
			// Get template xml string, if buffer isn't large enough to return it, then return the size of the string (plus one for null-termination)
			SAString sql; sql.Format(_T("SELECT pricelist FROM prn_pricelist_table WHERE id = %d"), nPricelistId);
			SACommand cmd(conn.conn, sql);
			cmd.Execute();
			while(cmd.FetchNext()) // Intentional while loop
			{
				if( nDesiredSize > cmd.Field(1).asString().GetLength() )
				{
					wcscpy_s(pData, nDesiredSize, cmd.Field(1).asString());
					ret = true;
				}
				
				// Always return actual size (plus null-term char)
				*nActualSize = cmd.Field(1).asString().GetLength() + 1;
			}
		}
	}
	catch( SAException &e )
	{
		SetLastMessage(e.ErrText());
	}

	return ret;
}

boolean GetStandData(const int nStandId, struct STANDDATA* pData)
{
	bool ret = false;
	DB_CONNECTION_DATA conn;

	try
	{
		if( GetConn(conn) )
		{
			// Get data for specified stand
			SAString sql;
			sql.Format(_T("SELECT ")
				_T("trakt_id,")
				_T("trakt_name [Best�nd],")
				_T("SUM(DISTINCT trakt_areal) [Areal],")
				_T("SUM(tdata_numof) [Antal_stam],")
				_T("(SELECT tdata_percent FROM esti_trakt_data_table WHERE tdata_trakt_id = trakt_id AND tdata_data_type = 1 AND tdata_spc_id = 1) [T%],")
				_T("(SELECT tdata_percent FROM esti_trakt_data_table WHERE tdata_trakt_id = trakt_id AND tdata_data_type = 1 AND tdata_spc_id = 2) [G%],")
				_T("(SELECT tdata_percent FROM esti_trakt_data_table WHERE tdata_trakt_id = trakt_id AND tdata_data_type = 1 AND tdata_spc_id = 3) [B%],")
				_T("SUM(tdata_gy) [Gy],")
				_T("CASE WHEN SUM(tdata_numof) <> 0 THEN SUM(tdata_da * tdata_numof) / SUM(tdata_numof) ELSE NULL END [Da],")
				_T("CASE WHEN SUM(tdata_m3sk_vol) <> 0 THEN SUM(tdata_hgv * tdata_m3sk_vol) / SUM(tdata_m3sk_vol) ELSE NULL END [Hgv],")
				_T("CASE WHEN SUM(tdata_numof) <> 0 THEN SUM(tdata_avg_hgt * tdata_numof) / SUM(tdata_numof) ELSE NULL END [Medelh�jd],")
				_T("SUM(tdata_m3sk_vol) [Volym_m3sk],")
				_T("SUM(tdata_m3fub_vol) [Volym_m3fub],")
				_T("CASE WHEN SUM(tdata_numof) <> 0 THEN SUM(tdata_m3sk_vol) / SUM(tdata_numof) ELSE NULL END [M.volym_m3sk],")
				_T("CASE WHEN SUM(tdata_numof) <> 0 THEN SUM(tdata_m3fub_vol) / SUM(tdata_numof) ELSE NULL END [M.volym_m3fub],")
				_T("ROUND(SUM(DISTINCT rot_post_value - rot_post_cost1 - rot_post_cost2 - rot_post_cost3 - rot_post_cost5 - rot_post_cost6 - rot_post_cost7 - rot_post_cost8), 0) [Rotnetto],")
				_T("ROUND(SUM(DISTINCT rot_post_cost1 + rot_post_cost2 + rot_post_cost3 + rot_post_cost5), 0) [Kostnad]")
				_T("FROM esti_trakt_table t ")
				_T("INNER JOIN esti_trakt_data_table d ON d.tdata_trakt_id = t.trakt_id ")
				_T("LEFT JOIN esti_trakt_rotpost_table r ON r.rot_trakt_id = t.trakt_id ")
				_T("WHERE trakt_id = %d ")
				_T("GROUP BY trakt_id, trakt_name")
				, nStandId);
			SACommand cmd(conn.conn, sql);
			cmd.Execute();

			while( cmd.FetchNext() ) // Intentional while loop
			{
				pData->id		= cmd.Field(1).asLong();
				wcscpy_s(pData->name, sizeof(pData->name) / sizeof(wchar_t), cmd.Field(2).asString());
				pData->area		= cmd.Field(3).asDouble();
				pData->numTrees	= cmd.Field(4).asLong();
				pData->pine		= cmd.Field(5).asDouble();
				pData->spruce	= cmd.Field(6).asDouble();
				pData->birch	= cmd.Field(7).asDouble();
				pData->gy		= cmd.Field(8).asDouble();
				pData->da		= cmd.Field(9).asDouble();
				pData->hgv		= cmd.Field(10).asDouble();
				pData->avgHgt	= cmd.Field(11).asDouble();
				pData->m3sk		= cmd.Field(12).asDouble();
				pData->m3fub	= cmd.Field(13).asDouble();
				pData->avgM3sk	= cmd.Field(14).asDouble();
				pData->avgM3fub	= cmd.Field(15).asDouble();
				pData->netto	= cmd.Field(16).asLong();
				pData->cost		= cmd.Field(17).asLong();
				ret = true;
			}
		}
	}
	catch( SAException &e )
	{
		SetLastMessage(e.ErrText());
	}

	return ret;
}

boolean RemoveStand(const int nStandId)
{
	bool ret = false;
	DB_CONNECTION_DATA conn;

	// Make sure we process only one request at a time
	if( !EnterCriticalSection(g_nLock) )
	{
		return false;
	}

	try
	{
		if( GetConn(conn) )
		{
			// Directly remove stand from db
			SAString sql; sql.Format(_T("DELETE FROM esti_trakt_table WHERE trakt_id = %d"), nStandId);
			SACommand cmd(conn.conn, sql);
			cmd.Execute();

			ret = true;
		}
	}
	catch( SAException &e )
	{
		SetLastMessage(e.ErrText());
	}

	LeaveCriticalSection(g_nLock);

	return ret;
}

boolean CheckLicence(const wchar_t* pUser)
{
	return _CheckLicence(pUser);
}

wchar_t* GetLastMessage()
{
	// We need to create a new string on the heap (this string will automatically be freed when this function return)
	// Note! Don't return any temp object (with additional data) like CString as it will result in a corruption of the heap upon freeing
	int size = g_strLastMessage.GetLength() + 1;
	wchar_t *pMessage = new wchar_t[size];
	wcscpy_s(pMessage, size, g_strLastMessage);
	return pMessage;
}
