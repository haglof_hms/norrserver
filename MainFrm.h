
// MainFrm.h : interface of the CMainFrame class
//

#pragma once
#include "ChildView.h"


class CMainFrame : public CFrameWnd
{
	
public:
	CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)

	BOOL LoadAppSettings();

	DB_CONNECTION_DATA m_DBConnection;
	SAConnection m_saConnection;

	CString m_strServer;
	CString m_strDatabase;
	CString m_strUsername;
	CString m_strPassword;
	BOOL m_bWindowsAuth;
	int m_nPort;

// Attributes
public:
	virtual DB_CONNECTION_DATA GetDBConnectionData()	{ return m_DBConnection; }
	int GetPort() { return m_nPort; }

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	CChildView    m_wndView;

// Generated message map functions
protected:
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	DECLARE_MESSAGE_MAP()

};


