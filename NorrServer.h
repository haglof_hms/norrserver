
// NorrServer.h : main header file for the NorrServer application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CNorrServerApp:
// See NorrServer.cpp for the implementation of this class
//

class CNorrServerApp : public CWinApp
{
public:
	CNorrServerApp();


// Overrides
public:
	virtual BOOL ExitInstance();
	virtual BOOL InitInstance();
	virtual int Run();

// Implementation

public:
	DECLARE_MESSAGE_MAP()

protected:
	HANDLE m_hRpcThread;
};

extern CNorrServerApp theApp;
extern BOOL runningService;


void* __RPC_USER midl_user_allocate(size_t size);
void __RPC_USER midl_user_free(void* p);

DWORD WINAPI RpcThread(LPVOID lpArgs);
