
// NorrServer.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include <locale.h>
#include "afxwinappex.h"
#include "LicProtectorEasyGo270.h"
#include "NorrServer.h"
#include "MainFrm.h"
#include "Utilities.h"
#include "Intermediate/NorrServer_h.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CNorrServerApp

BEGIN_MESSAGE_MAP(CNorrServerApp, CWinApp)
END_MESSAGE_MAP()


// CNorrServerApp construction

CNorrServerApp::CNorrServerApp():
m_hRpcThread(NULL)
{
}

CNorrServerApp theApp;


BOOL CNorrServerApp::InitInstance()
{
	CWinApp::InitInstance();

	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object
	CMainFrame* pFrame = new CMainFrame;
	if (!pFrame)
		return FALSE;
	m_pMainWnd = pFrame;

	// create and load the frame with its resources
	if( !pFrame->LoadFrame(IDR_MAINFRAME,
		WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,
		NULL) )
	{
		WriteToEventLog(EVENTLOG_ERROR_TYPE, _T("Skapandet av MainFrame misslyckades."));
		return false;
	}

	// Create licence automation object
	if( !g_proxyLicProtector.CreateDispatch(_T("LicProtectorEasyGo.LicProtectorEasyGo270")) )
	{
		WriteToEventLog(EVENTLOG_ERROR_TYPE, _T("Automation object not found (LicProtector270.dll)."));
		return false;
	}

	int ret;
	g_proxyLicProtector.put_SecurityLevel(0);
	if( (ret = g_proxyLicProtector.Prepare(GetAppDir() + LicenceFile, BasicKey)) )
	{
		WriteToEventLog(EVENTLOG_ERROR_TYPE, g_proxyLicProtector.GetErrorMessage(ret));
		g_proxyLicProtector.ReleaseDispatch();
		return false;
	}

	// Create RPC listening thread
	if( !(m_hRpcThread = ::CreateThread(NULL, 0, RpcThread, reinterpret_cast<LPVOID>(pFrame->GetPort()), 0, NULL)) )
	{
		CString err; err.Format(_T("RPC-tr�den kunde inte skapas. Felkod %d."), GetLastError());
		WriteToEventLog(EVENTLOG_ERROR_TYPE, err);
		return FALSE;
	}

	// Set up a timer to check if RPC thread is alive
	::SetTimer(NULL, 0, 1000, NULL);

	// Make sure locale is set (system default; used by indata, template parser, etc.)
	::setlocale(LC_ALL, "");

	WriteToEventLog(EVENTLOG_INFORMATION_TYPE, _T("Tj�nsten har startats."));

	return TRUE;
}

BOOL CNorrServerApp::ExitInstance()
{
	g_proxyLicProtector.Quit();
	g_proxyLicProtector.ReleaseDispatch();

	WriteToEventLog(EVENTLOG_INFORMATION_TYPE, _T("Tj�nsten har stoppats."));

	return CWinApp::ExitInstance();
}

int CNorrServerApp::Run()
{
	while(runningService)
	{
		// Make sure RPC listening thread is alive
		if( ::WaitForSingleObject(m_hRpcThread, 0) != WAIT_TIMEOUT )
		{
			DWORD dwCode;
			if( GetExitCodeThread(m_hRpcThread, &dwCode) )
			{
				CString err; err.Format(_T("RPC-tr�den har avslutats med koden %d."), dwCode);
				WriteToEventLog(EVENTLOG_ERROR_TYPE, err);
			}
			else
			{
				WriteToEventLog(EVENTLOG_ERROR_TYPE, _T("RPC-tr�den har avslutas med ok�nd returkod."));
			}
			return(-1);
		}

		Sleep(1000);
	}

	return 0;
}


DWORD WINAPI RpcThread(LPVOID lpArgs)
{
	RPC_STATUS status;
	wchar_t szPort[20];

	_itow_s(reinterpret_cast<int>(lpArgs), szPort, sizeof(szPort) / sizeof(TCHAR), 10);

	// Uses the protocol combined with the endpoint for receiving remote procedure calls.
	status = RpcServerUseProtseqEp(
		reinterpret_cast<RPC_WSTR>(L"ncacn_ip_tcp"),// Use TCP/IP protocol.
		RPC_C_PROTSEQ_MAX_REQS_DEFAULT,				// Backlog queue length for TCP/IP.
		reinterpret_cast<RPC_WSTR>(szPort),			// TCP/IP port to use.
		NULL);										// No security.
	if (status) return status;

	// Registers the interface.
	status = RpcServerRegisterIfEx(
		NorrServer_v1_0_s_ifspec, // Interface to register.
		NULL, // Use the MIDL generated entry-point vector.
		NULL, // Use the MIDL generated entry-point vector.
		RPC_IF_ALLOW_CALLBACKS_WITH_NO_AUTH,
		RPC_C_LISTEN_MAX_CALLS_DEFAULT,
		NULL);
	if (status) return status;

	// Start to listen for remote procedure calls for all registered interfaces.
	// This call will not return until RpcMgmtStopServerListening is called.
	status = RpcServerListen(
		1,								// Recommended minimum number of threads.
		RPC_C_LISTEN_MAX_CALLS_DEFAULT,	// Recommended maximum number of threads.
		FALSE);							// Start listening now.
	if (status) return status;

	return 0;
}

// Memory allocation function for RPC.
void* __RPC_USER midl_user_allocate(size_t size)
{
	return malloc(size);
}

// Memory deallocation function for RPC.
void __RPC_USER midl_user_free(void* p)
{
	free(p);
}
