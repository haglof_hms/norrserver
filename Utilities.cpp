#include "stdafx.h"
#include "LicProtectorEasyGo270.h"
#include "MainFrm.h"
#include "NorrServer.h"
#include "Utilities.h"

CLicProtectorEasyGo270 g_proxyLicProtector;
CString g_strLastMessage;


// Helper functions

bool EnterCriticalSection(int &semaphore)
{
	int tries = 0;

	const int timeout = 5000; // timeout in ms
	const int interval = 100; // interval between tries in ms

	while( semaphore > 0 )
	{
		Sleep(interval);
		if( (++tries * interval) >= timeout  )
		{
			SetLastMessage(_T("Servern �r upptagen med ett annat anrop."));
			return false;
		}
	}

	semaphore++;
	return true;
}

void LeaveCriticalSection(int &semaphore)
{
	semaphore--;
}

bool GetConn(DB_CONNECTION_DATA &conn)
{
	CMainFrame *pFrame = ((CMainFrame*)AfxGetApp()->GetMainWnd());
	if( pFrame )
	{
		// Make sure connection is ok
		conn = pFrame->GetDBConnectionData();
		if( conn.conn && conn.conn->isConnected() )
		{
			return true;
		}
		else
		{
			SetLastMessage(_T("NorrServer �r inte ansluten till databasen."));
		}
	}
	else
	{
		SetLastMessage(_T("MainFrame �r null."));
	}

	return false;
}

CString GetTempFilePath()
{
	CString path;
	CString file;
	if( GetTempPath(MAX_PATH, path.GetBuffer(MAX_PATH+1)) )
	{
		if( GetTempFileName(path, NULL, 0, file.GetBuffer(MAX_PATH+1)) )
		{
			path.ReleaseBuffer();
			file.ReleaseBuffer();
			return file;
		}
	}

	return NULL;
}

CString GetAppDir()
{
	CString dir(theApp.m_pszHelpFilePath);
	dir = dir.Left(dir.ReverseFind('\\') + 1);
	return dir;
}

void WriteToEventLog(WORD type, LPCTSTR msg)
{
    HANDLE hEventLog = RegisterEventSource(NULL, AfxGetApp()->m_pszExeName);
    if( !hEventLog ) return;
 
	// Write message to event log
    ReportEvent(hEventLog, type, 0, 0, NULL, 1, 0, &msg, NULL);
 
    DeregisterEventSource(hEventLog);
}

void SetLastMessage(LPCTSTR msg)
{
	g_strLastMessage = msg;
}

bool UpdateLastMessage(HMODULE hModule)
{
	// Get message buffer from current module
	GetMessageLogFunction logfunc = (GetMessageLogFunction)GetProcAddress((HMODULE)hModule, "GetMessageLog");
	if( logfunc )
	{
		logfunc(g_strLastMessage);
		return true;
	}
	else
	{
		SetLastMessage(_T("Varning! Det gick inte att h�mta meddelanden fr�n modulen."));
		return false;
	}
}

bool _CheckLicence(LPCTSTR user)
{
	bool found = false;
	static CList<LICENCE, LICENCE&> licences;
	POSITION pos, prev;

	// Tamper check
	if( g_proxyLicProtector.get_LicenceTampered() )
	{
		SetLastMessage(_T("Licensfilen �r korrupt."));
		return false;
	}

	// Licence check, we don't care about concurrent licences here, just make sure it's valid
	g_proxyLicProtector.RemoveAllItems(ModuleId);
	if( g_proxyLicProtector.Validate(ModuleId, user, false) )
	{
		SetLastMessage(_T("Licensen �r inte giltig."));
		return false;
	}

	//  Now check number of concurrent licences
	pos = licences.GetHeadPosition();
	while( pos )
	{
		prev = pos;
		LICENCE &lic = licences.GetNext(pos);

		// Check if this seat belongs to current user
		if( lic.user.Compare(user) == 0 )
		{
			// Update last used time
			lic.time = GetTickCount();
			found = true;
			break;
		}
		// Check if this seat has timed out
		else if( lic.time + LicenceTimeout < GetTickCount() || lic.time > GetTickCount() ) // GetTickCount will wrap around to 0 every 49.7 days, thus the extra check
		{
			licences.RemoveAt(prev);
		}
	}

	// Add user to licence list (if not already exist)
	if( !found )
	{
		// Check if there are any free licences left
		if( licences.GetCount() >= g_proxyLicProtector.TotalLicences(ModuleId) )
		{
			SetLastMessage(_T("Det finns f�r n�rvarande inga lediga licenser."));
			return false;
		}

		// Reserve a licence seat
		LICENCE lic;
		lic.time = GetTickCount();
		lic.user = user;
		licences.AddTail(lic);
	}

	return true;
}
