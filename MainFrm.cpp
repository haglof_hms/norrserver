
// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "NorrServer.h"
#include "MainFrm.h"
#include "Utilities.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()

// CMainFrame construction/destruction

CMainFrame::CMainFrame():
m_bWindowsAuth(FALSE),
m_nPort(0)
{
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create a view to occupy the client area of the frame
	if (!m_wndView.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		WriteToEventLog(EVENTLOG_ERROR_TYPE, _T("Det gick inte att skapa bakgrundsf�nstret."));
		return -1;
	}

	// Load db settings
	if( !LoadAppSettings() )
	{
		WriteToEventLog(EVENTLOG_ERROR_TYPE, _T("Kunde inte l�sa in inst�llningarna f�r tj�nsten."));
		return -1;
	}

	// Set up database connection
	SAString sDBString;
	try
	{
		sDBString = m_strServer + _T("@") + m_strDatabase;
		if( m_bWindowsAuth )	m_saConnection.Connect(sDBString, _T(""), _T(""), SA_SQLServer_Client);
		else					m_saConnection.Connect(sDBString, m_strUsername.GetBuffer(), m_strPassword.GetBuffer(), SA_SQLServer_Client);
		if( !m_saConnection.isConnected() )
		{
			CString err; err.Format(_T("Det gick inte att ansluta till databasen %s."), sDBString);
			WriteToEventLog(EVENTLOG_ERROR_TYPE, err);
			return -1;
		}
		m_DBConnection.conn = &m_saConnection;
		m_DBConnection.admin_conn = NULL;
	}
	catch( SAException &e )
	{
		CString err; err.Format(_T("Ett fel intr�ffade n�r tj�nsten skulle ansluta till databasen (%s): %s"), sDBString, e.ErrText());
		WriteToEventLog(EVENTLOG_ERROR_TYPE, err);
		return -1;
	}

	return 0;
}

BOOL CMainFrame::LoadAppSettings()
{
	// Load db settings from ini file
	CString strFile;
	TCHAR szTmp[MAX_PATH];
	int len;

	// Construct ini file path (use application directory)
	GetModuleFileName(NULL, szTmp, sizeof(szTmp) / sizeof(TCHAR));
	strFile = szTmp;
	strFile = strFile.Left(strFile.ReverseFind('\\') + 1) + _T("NorrServer.ini");

	// Server
	// -Port
	m_nPort = GetPrivateProfileInt(_T("Server"), _T("Port"), 0, strFile);
	if( !m_nPort ) return FALSE;

	// Database
	// -Server
	len = GetPrivateProfileString(_T("Database"), _T("Server"), NULL, szTmp, sizeof(szTmp) / sizeof(TCHAR), strFile);
	if( !len ) return FALSE;
	m_strServer = szTmp;

	// -Database
	len = GetPrivateProfileString(_T("Database"), _T("Database"), NULL, szTmp, sizeof(szTmp) / sizeof(TCHAR), strFile);
	if( !len ) return FALSE;
	m_strDatabase = szTmp;

	// -Username
	m_bWindowsAuth = FALSE;	// server auth
	len = GetPrivateProfileString(_T("Database"), _T("Username"), NULL, szTmp, sizeof(szTmp) / sizeof(TCHAR), strFile);
	if( !len ) m_bWindowsAuth = TRUE;	// windows auth
	m_strUsername = szTmp;

	// -Password
	len = GetPrivateProfileString(_T("Database"), _T("Password"), NULL, szTmp, sizeof(szTmp) / sizeof(TCHAR), strFile);
	if( !len ) m_bWindowsAuth = TRUE;	// windows auth
	m_strPassword = szTmp;

	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
}

// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


// CMainFrame message handlers

BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	// Return db connection to requesting window
	if (pCopyDataStruct->cbData == sizeof(DB_CONNECTION_DATA))
	{
		COPYDATASTRUCT HSData;
		memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &m_DBConnection;
		m_DBConnection.conn = &m_saConnection;
		m_DBConnection.admin_conn = NULL;

		::SendMessage(pWnd->GetSafeHwnd(),WM_COPYDATA,(WPARAM)this->GetSafeHwnd(), (LPARAM)&HSData);
	}

	return CFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}

void CMainFrame::OnSetFocus(CWnd* /*pOldWnd*/)
{
	// forward focus to the view window
	m_wndView.SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// let the view have first crack at the command
	if (m_wndView.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	// otherwise, do default handling
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}
